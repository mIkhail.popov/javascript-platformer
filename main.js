var canvas = document.querySelector('canvas');
var render = canvas.getContext('2d');

canvas.style.left = "0px";
canvas.style.top = "0px";
canvas.style.position = "absolute";


window.onresize = function(){
    canvas.width = innerWidth;
    canvas.height = innerHeight;
    drawLoop();
}

var objects = [];


let wall = new Wall(0, 400);

for(var i=0; i<32; i++){
    new Wall(i*32, 400);
}


window.onresize();
var player = new Player(30, 300);

function loop() {
    stepLoop();
    drawLoop();
}
function stepLoop() {
    for(var i=0; i<objects.length; i++) {
        if(objects[i].step) objects[i].step();
    }
}
function drawLoop() {
    background(25, 25, 25);
    for(var i=0; i<objects.length; i++) {
        if(objects[i].draw) objects[i].draw();
    }
}

var target_fps = 30;
setInterval(loop, 1000 / target_fps);

