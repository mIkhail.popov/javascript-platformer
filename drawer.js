function rectangle(x, y, w, h) {
    render.beginPath();
    render.rect(x, y, w, h);
    render.fill();
    render.stroke();
}
function fillcolor(r, g, b){
    r = String(r);
    g = String(g);
    b = String(b);
    render.fillStyle = "rgb("+r+", "+g+", "+b+")";
}
function strokeSize(size){
    render.lineWidth = String(size);
}
function drawimage() {
    var img = new Image();
    img.src = "./player.png";
    render.drawImage(img, player.x, player.y, player.size, player.size2);
}
function background(r, g, b){
    fillcolor(r, g, b);
    rectangle(0, 0, canvas.width, canvas.height);
}
function strokecolor(r, g, b){
    r = String(r);
    g = String(g);
    b = String(b);
    render.strokeStyle = "rgb("+r+", "+g+", "+b+")";
}
function noStroke() {
    render.strokeStyle = "rgba(0, 0, 0, 0)";
}

function noFill() {
    render.fillStyle = "rgba(0, 0, 0, 0)";
}

function line(x1, x1, x2, x2) {
    render.moveTo(x1, x1);
    render.lineTo(x2, x2);
    render.stroke();
}   