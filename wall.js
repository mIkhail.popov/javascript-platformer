var walls = [];
class Wall {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.size = 32;
        this.size2 = 32;
        objects.push(this);
        objects.push(this);
    }
    draw() {
        strokecolor(255, 255, 255);
        noFill();
        strokeSize(2);
        rectangle(this.x, this.y, this.size, this.size2);
    }
}